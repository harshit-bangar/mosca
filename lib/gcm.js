"use strict";
var gcm = require('node-gcm');

var message = new gcm.Message();
var urgentMessage = new gcm.Message({
  timeToLive: 0,
  priority: 'high'
});
var sender = new gcm.Sender('AIzaSyABLkwOwfXrjLzDI_HqEadlNNnWfNwg1rQ');

/*
 * If user offline, send it wake up message through gcm
 */

var Gcm = function () {
  if (!(this instanceof Gcm)) {
    return new Gcm();
  }
};

Gcm.prototype.wire = function (server) {

  // wake for call
  server.on('wakeCall', function(registrationToken) {
    if (typeof registrationToken === "undefined") {
      return;
    }
    server.emit("infolog", "gcm wakeCall is called for: " + registrationToken);
    if (typeof registrationToken === 'string') {
      registrationToken = [registrationToken]

    }
    wakeUrgent(registrationToken, server)
  });

  // wake for non call messages
  server.on('wake', function(registrationToken) {
    if (typeof registrationToken === "undefined") {
      return;
    }
    server.emit("infolog", "gcm wake is called for: " + registrationToken);
    if (typeof registrationToken === 'string') {
      registrationToken = [registrationToken]

    }
    wake(registrationToken, server)
  });
};

var wake = function (registrationToken, server) {
  sender.send(message, { registrationTokens: registrationToken }, 3, function (err, response) {
    if(err) {
      console.error(err);
      server.emit("errorlog", err);
    } else {
      server.emit("infolog", response);
    }
  });
};

var wakeUrgent = function (registrationToken, server) {
  sender.sendNoRetry(urgentMessage, { registrationTokens: registrationToken }, function(err, response) {
    if(err) {
      console.error(err);
      server.emit("errorlog", err);
    } else {
      server.emit("infolog", response);
    }
  });
};

module.exports = Gcm;
