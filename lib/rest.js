/**
 * Created by harshit on 3/6/16.
 */
"use strict";

var async = require("async");
var EventEmitter = require("events").EventEmitter;
var util = require("util");

/**
 *
 * A rest server for mosca.
 * @api Public
 */
function Rest() {
  if (!(this instanceof Rest)) {
    return new Rest();
  }
}

util.inherits(Rest, EventEmitter);

Rest.prototype.wire = function (server) {
  var express = require('express');
  var app = express();
  var bodyParser = require('body-parser');
  app.use(bodyParser.json());
  app.use(bodyParser.text({type: 'text/plain'}));

  app.get('/', function (req, res) {
    res.send('Hello World')
  });

  app.post('/addUser', function (req, res) {
    if (!req.body) return res.sendStatus(404);
    var username = req.body.username;
    var password = req.body.password;
    if (!username || !password) {
      return res.status(400).send('username or password is missing');
    }
    server.addUser(username, password);
    res.status(201).end();
  });

  app.put('/updateRegistrationId/:username', function (req, res) {
    if (!req.body) return res.sendStatus(404);
    var username = req.params.username;
    var registrationId = req.body;
    if (!username || !registrationId) {
      return res.status(400).send('username or registrationIdis missing');
    }
    server.updateRegistrationId(username, registrationId);
    res.status(204).end();
  });

  app.listen(3000)
};

module.exports = Rest;
