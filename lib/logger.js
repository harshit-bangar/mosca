/**
 * Created by harshitbangar on 03/07/16.
 */
var winston = require('winston');
require('winston-loggly');

winston.add(winston.transports.Loggly, {
    token: "271aea8b-5814-4592-8015-113efc116fb2",
    subdomain: "harshitbangar",
    tags: ["Winston-NodeJS"],
    json:true
});

var Logger = function () {
    if (!(this instanceof Logger)) {
        return new Logger();
    }
};

Logger.prototype.wire = function (server) {
    server.on('infolog', function (log) {
        winston.log('info', log);
    });
    server.on("errorlog", function (log) {
        winston.log('error', log);
    })
};

Logger.prototype.info = function (log) {
    winston.log('info', log);
};

Logger.prototype.error = function (log) {
    winston.log('error', log);
};


module.exports = Logger;