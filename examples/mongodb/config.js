var mosca = require('../../');

module.exports = {
  id: 'mymosca', // used to publish in the $SYS/<id> topicspace
  stats: false, // publish stats in the $SYS/<id> topicspace
  logger: {
    level: 'debug'
  },
  backend: {
    type: 'mongodb',
    url: "mongodb://ec2-54-169-250-141.ap-southeast-1.compute.amazonaws.com:27017/mosca",
    offlineMessageTimeout: 3600 * 1000 * 10000,
    ttl: {
      subscriptions: 3600 * 1000 * 100000,
      packets: 1000 * 60 * 10
    }
  },
  persistence: {
    factory: mosca.persistence.Mongo,
    url: "mongodb://ec2-54-169-250-141.ap-southeast-1.compute.amazonaws.com:27017/mosca",
    offlineMessageTimeout: 3600 * 1000 * 200,
    ttl: {
      subscriptions: 3600 * 1000 * 100000,
      packets: 1000 * 60 * 10
    }
  }
};
